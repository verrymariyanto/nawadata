// import logo from './logo.svg';
import './App.css';
import {Routes, Route, BrowserRouterProps} from 'react-router-dom'
// import {browserHistory} from 'react-router'
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './views/Home'
import Product from './views/Product'
import Transaction from './views/Transaction';
import CustomerDetail  from './views/CustomerDetail';
import ProductDetail from './views/ProductDetail';
import TransactionDetail from './views/DetailTransaction';

// import CustomerDetail from './views/CustomerDetail';

function App() {
  return (
    <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/Product' element={<Product />} />
        <Route path='/Transaction' element={<Transaction />} />
        <Route path='/CustomerDetail' element={<CustomerDetail />} />
        <Route path='/ProductDetail' element={<ProductDetail />} />
        <Route path='/TransactionDetail' element={<TransactionDetail />} />
    </Routes>
  );
}

export default App;
