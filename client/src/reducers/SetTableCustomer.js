const setTabelCustomer = (state = [], action)=>{
    switch(action.type){
        case 'SET_TABLE':
            return state = [...action.payload];
        case 'ADD_CUSTOMER':
            return state = [...state, ...action.payload];
        default :
            return state;
    }
}
export default setTabelCustomer;