import React, { useState, useRef, useEffect } from "react";
// import { useDispatch, useSelector } from "react-redux";
// import { setTabelCustomer, addCustomer } from "../actions";
import { NavbarWeb } from "../components/Navbar";
import { Container, Table, Button, Modal, Form } from "react-bootstrap";
import DB_CUSTOMER from "../data/pelanggan.json";
import TabelData from "../components/TabelData";
import { useNavigate, useLocation } from "react-router-dom";

export default function Home() {
  // ----------------------------------------------------------LOCAL STATE--------------------------------------------------
  const [show, setShow] = useState(false);
  const [data, setData] = useState(DB_CUSTOMER);
  const [newData, setNewData] = useState({});
  const navigate = useNavigate();
  let nameRef = useRef();
  let addressRef = useRef();
  let phoneRef = useRef();
  let location = useLocation();
  // ----------------------------------------------------------USE EFFECT---------------------------------------------------
  useEffect(() => {
    if(location.state !== null){
      setData([...data,location.state]);
    }
  }, []);
  // ---------------------------------------------------------------FUNCTION---------------------------------------------------
  const toDetail = (id) => {
    let stateID = data.filter((el, idx) => {
      return el.id === id;
    });
    console.log(id, "---VS---", stateID);
    navigate(`/CustomerDetail`, { state: stateID[0] });
  };
  const deleteCustomer = (id) => {
    console.log(id, "id from delete parent");
    let dataTemp = data.filter((el) => {
      return el.id !== Number(id);
    });
    setData(dataTemp);
  };
  const addCustomer = () => setShow(true);
  const handleClose = () => setShow(false);
  const submitCustomer = () => {
    let newCustomer = {
      id: data[data.length - 1].id + 1,
      nama: nameRef.current.value,
      alamat: addressRef.current.value,
      telp: phoneRef.current.value
      .split("")
      .filter((el, id) => {
        return el !== " " && /^[0-9]+$/.test(Number(el));
      })
      .join("")
      .trim(),
    };
    setData([...data, newCustomer]);
    setShow(false);
  };
  console.log(data,"masuk data baru");
  // ---------------------------------------------------------RENDERING--------------------------------------------------

  return (
    <>
      <NavbarWeb />
      <Container>
        <h3>DATA PELANGGAN</h3>
        {/* ---------------------------------------BUTTON ADD CUSTOMER------------------------------------------ */}
        <Button variant="primary" type="submit" onClick={addCustomer}>
          add customer
        </Button>
        {/* -------------------------------------MODAL FOR ADD CUSTOMER------------------------------------------- */}
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                {/* --------------------------------NAME-------------------------------- */}
                <Form.Label>Nama</Form.Label>
                <Form.Control type="text" autoFocus ref={nameRef} />
                {/* --------------------------------ADDRESS------------------------------- */}

                <Form.Label>Alamat</Form.Label>
                <Form.Control type="text" autoFocus ref={addressRef} />
                {/* ----------------------------------------PHONE------------------------------------ */}

                <Form.Label>No Telp</Form.Label>
                <Form.Control type="text" autoFocus ref={phoneRef} />
              </Form.Group>
            </Form>
          </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                  Cancel
                </Button>
                <Button variant="primary" onClick={submitCustomer}>
                  Add Customer
                </Button>
              </Modal.Footer>
        </Modal>
        <hr />
        {/* ----------------------------------------------TABLE---------------------------------------------------- */}
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>No Telp</th>
              <th>Action</th>
            </tr>
          </thead>
          <TabelData
            data={data}
            detail={toDetail}
            del={deleteCustomer}
          ></TabelData>
        </Table>
      </Container>
    </>
  );
}
