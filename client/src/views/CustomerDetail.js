import React, { useState, useRef, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Button, Modal, Form } from "react-bootstrap";

export default function CustomerDetail() {
  //   -----------------------------------------------------------LOCAL STATE----------------------------------------------
  let location = useLocation();
  let navigate = useNavigate();
  const [editModal, setEditModal] = useState(false);
  const [data, setData] = useState({});
  let nameRef = useRef();
  let addressRef = useRef();
  let phoneRef = useRef();
  useEffect(() => {
    setData(location.state);
  }, []);
  useEffect(()=>{},[data]);
  // ---------------------------------------------------------------FUNCTION--------------------------------------------------
  console.log(data,"---data----");
  //   --------------MODAL FUNCTION--------------
  const showModal = () => {
    setEditModal(true);
  };
  const handleClose = () => setEditModal(false);

  //   --------------CUSTOMER FUNCTION--------------
  const goToHome = () => {
    navigate("/", { state: data });
  };

  const submitCustomer = () => {
    console.log(data,"data beofre----------------");
    let editCustomer = {
      id: data.id,
      nama: nameRef.current.value === "" ? data.nama : nameRef.current.value,
      alamat:
        addressRef.current.value === ""
          ? data.alamat
          : addressRef.current.value,
      telp:
        phoneRef.current.value === ""
          ? data.telp
          : phoneRef.current.value
              .split("")
              .filter((el) => {
                return el !== " " && /^[0-9]+$/.test(Number(el));
              })
              .join("")
              .trim(),
    };
    console.log(editCustomer, "new customer");
    setData(editCustomer);
    setEditModal(false);
  };
  // ---------------------------------------------------------RENDERING--------------------------------------------------
  return (
    <>
      {/* ----------------------------------------------------NAVBAR------------------------------------------- */}

      {/* <NavbarWeb /> */}
      {/* -------------------------------------------------DETAIL DATA------------------------------------------- */}
      <Button onClick={goToHome}>Go To Home</Button>
      <h2>DETAIL :</h2>
      {/* {[data].map((el, idx)=>{
        return (<div>{Object.entries(el).map((e,id)=>{
          if(id !== 0){
            return (<h4>{e[0]} : {e[1]}</h4>)
          }
        })}</div>)
      })} */}
      <h4>nama : {data.nama}</h4>
      <h4>alamat : {data.alamat}</h4>
      <h4>no Telp : {data.telp}</h4>

      <div>
        <Button style={{ marginRight: "5px" }} onClick={showModal}>
          edit
        </Button>
      </div>
      {/* -------------------------------------MODAL FOR ADD CUSTOMER------------------------------------------- */}
      <Modal show={editModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label> Nama: {data.nama} </Form.Label>
              <Form.Control type="text" autoFocus ref={nameRef} />

              <Form.Label> Alamat: {data.alamat} </Form.Label>
              <Form.Control type="text" autoFocus ref={addressRef} />

              <Form.Label> No Telp: {data.telp} </Form.Label>
              <Form.Control type="text" autoFocus ref={phoneRef} />
              
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={submitCustomer}>
            Edit
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
