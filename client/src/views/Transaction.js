import React, { useEffect, useState, useRef } from "react";
import { Container, Table, Button, Modal, Form } from "react-bootstrap";
import TabelData from "../components/TabelData";
import {NavbarWeb} from '../components/Navbar'
import DB_TRANSACTION from '../data/transaksi'
import { useNavigate, useLocation } from "react-router-dom";



export default function Transaction() {
  const [data, setData] = useState(DB_TRANSACTION);
  let navigate = useNavigate()

  const toDetail = (e)=>{
    let id = e
    console.log(e,"----EEE");
    let stateID = data.filter((el, idx) => {
      return el.id === id;
    });
    navigate(`/TransactionDetail`, { state: stateID[0] });
  }
  return (
    <>
        <NavbarWeb></NavbarWeb>
        <h3>DATA TRANSAKSI</h3>
        {/* TABLE */}
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>Kode Transaksi</th>
              <th>Tanggal</th>
              <th>Pelanggan</th>
              <th>Total</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {data.map((el,idx)=>{
              return (
              <tr>
                <td>{idx+1}</td>
                <td>{el.kode}</td>
                <td>{el.tanggal}</td>
                <td>{el.pelanggan}</td>
                <td>{el.total}</td>
                <td><a type="button" onClick={()=>{return toDetail(el.id)}} value={el.id}>detail</a></td>
              </tr>)
            })}
          </tbody>
        </Table>
    </>
  )
}
