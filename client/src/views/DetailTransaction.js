import React, { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Button, Container, Table } from "react-bootstrap";

export default function DetailTransaction() {
  let location = useLocation();
  let navigate = useNavigate();
  const [data, setData] = useState({});
  const [dataProduct, setDataProduct] = useState([]);
  useEffect(() => {
    setData(location.state);
    let temp = Object.entries(location.state.produk);
    setDataProduct(temp);
  }, []);

  const goToTransaction = () => {
    navigate("/Transaction");
  };
  return (
    <Container>
      <h1>Detail Transaksi</h1>
      <hr />
      <div style={{ marginTop: "15px" }}>
        <h4 style={{ float: "right" }}>tanggal : {data.tanggal}</h4>
        <h4>Kode Transaksi : {data.kode} </h4>
        <h4>Pelanggan : {data.pelanggan}</h4>
        <h4>No Telp : </h4>
      </div>
      <hr />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>No</th>
            <th>Kode Transaksi</th>
            <th>Harga Satuan</th>
            <th>Jumlah</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {dataProduct.map((el, idx) => {
            return (
              <tr>
                <td>{idx + 1}</td>
                <td>{el[0]}</td>
                {Object.values(el[1]).map((e, id) => {
                  return (
                    <>
                      <td>{e}</td>
                    </>
                  );
                })}
                <td>{data.total}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
      <Button onClick={goToTransaction}>go back</Button>
    </Container>
  );
}
