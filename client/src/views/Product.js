import React, { useEffect, useState, useRef } from "react";
import { Container, Table, Button, Modal, Form } from "react-bootstrap";
import TabelData from "../components/TabelData";
import { NavbarWeb } from "../components/Navbar";
import DB_Product from "../data/produk.json";
import { useNavigate, useLocation } from "react-router-dom";

const toRupiah = (str) => {
  let result = str.split("").reverse();
  let Rp = [];
  let count = 0;
  for (let i = 0; i < result.length; i++) {
    if (count === 3) {
      Rp.push(".");
      count = 0;
    } else {
      Rp.push(result[i]);
      count++;
    }
  }
  return `Rp ${Rp.reverse().join("")}`;
};

export default function Product() {
  const [data, setData] = useState(DB_Product);
  const [show, setShow] = useState(false);
  let nameRef = useRef();
  let priceRef = useRef();
  let descriptionRef = useRef();
  let navigate = useNavigate();
  let location = useLocation();
  // ----------------------------------------------------------USE EFFECT---------------------------------------------------
  useEffect(() => {
    if (location.state !== null) {
      // console.log(location.state, "---location---");
      // let dataFromDetail = location.state;
      // for (let i = 0; i < data.length; i++) {
      //   if (dataFromDetail.id === data[i].id) {
      //     data[i] = location.state;
      //   }
      // }
      setData([...data, location.state])
    }
  },[]);
  // -------------------------------------------------------FUNCTION--------------------------------------------------
  const deleteElement = (id) => {
    let dataTemp = data.filter((el) => {
      return el.id !== Number(id);
    });
    setData(dataTemp);
  };

  const toDetail = (id) => {
    let stateID = data.filter((el, idx) => {
      return el.id === id;
    });
    navigate(`/ProductDetail`, { state: stateID[0] });
  };
  const addProduct = () => {
    setShow(true);
  };
  const submitProduct = () => {
    let newCustomer = {
      id: data[data.length - 1].id + 1,
      produk: nameRef.current.value,
      harga: toRupiah(priceRef.current.value),
      deskripsi: descriptionRef.current.value,
    };
    setData([...data, newCustomer]);
    setShow(false);
  };
  const handleClose = () => setShow(false);
  return (
    <>
      <NavbarWeb></NavbarWeb>
      <Container>
        <h3>DATA PRODUK</h3>
        {/* ---------------------------------------BUTTON ADD CUSTOMER------------------------------------------ */}
        <Button variant="primary" type="submit" onClick={addProduct}>
          tambah produk
        </Button>
        {/* -------------------------------------MODAL FOR ADD CUSTOMER------------------------------------------- */}
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                {/* --------------------------------NAME-------------------------------- */}
                <Form.Label>Nama produk</Form.Label>
                <Form.Control type="text" autoFocus ref={nameRef} />
                {/* --------------------------------ADDRESS------------------------------- */}

                <Form.Label>harga</Form.Label>
                <Form.Control type="text" autoFocus ref={priceRef} />
                {/* ----------------------------------------PHONE------------------------------------ */}

                <Form.Label>Deskripsi</Form.Label>
                <Form.Control
                  type="text"
                  autoFocus
                  ref={descriptionRef}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                  Cancel
                </Button>
                <Button variant="primary"  onClick={submitProduct}>
                  Add product
                </Button>
              </Modal.Footer>
        </Modal>
        <hr />
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>harga</th>
              <th>Deskripsi</th>
              <th>Action</th>
            </tr>
          </thead>
          <TabelData
            data={data}
            detail={toDetail}
            del={deleteElement}
          ></TabelData>
        </Table>
      </Container>
    </>
  );
}
