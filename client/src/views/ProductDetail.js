import React, { useState, useRef, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Button, Modal, Form } from "react-bootstrap";

const toRupiah = (str) => {
  let result = str.split("").reverse();
  let Rp = [];
  let count = 0;
  for (let i = 0; i < result.length; i++) {
    if (count === 3) {
      Rp.push(".");
      count = 0;
    } else {
      Rp.push(result[i]);
      count++;
    }
  }
  return `Rp ${Rp.reverse().join('')}`
};
export default function ProductDetail() {
  //   -----------------------------------------------------------LOCAL STATE----------------------------------------------
  let location = useLocation();
  let navigate = useNavigate();
  const [editModal, setEditModal] = useState(false);
  const [data, setData] = useState({});
  let nameRef = useRef();
  let priceRef = useRef();
  let descriptionRef = useRef();
  useEffect(() => {
    setData(location.state);
  }, []);
  // ---------------------------------------------------------------FUNCTION--------------------------------------------------
  //   --------------MODAL FUNCTION--------------
  const showModal = () => {
    setEditModal(true);
  };
  const handleClose = () => setEditModal(false);

  //   --------------CUSTOMER FUNCTION--------------
  const goToHome = () => {
    navigate("/Product", { state: data });
  };

  const submitProduct = () => {
    let editProduct = {
      id: data.id,
      produk:
        nameRef.current.value === "" ? data.produk : nameRef.current.value,
      harga:
        priceRef.current.value === "" ? data.harga : toRupiah(priceRef.current.value),
      deskripsi:
        descriptionRef.current.value === ""
          ? data.deskripsi
          : descriptionRef.current.value,
    };
    console.log(editProduct, "edit product");
    setEditModal(false);
    setData((prev) => (prev = editProduct));
  };
  // ---------------------------------------------------------RENDERING--------------------------------------------------
  return (
    <>
      {/* ----------------------------------------------------NAVBAR------------------------------------------- */}

      {/* <NavbarWeb /> */}
      {/* -------------------------------------------------DETAIL DATA------------------------------------------- */}
      <Button onClick={goToHome}>Go To Home</Button>
      <h2>DETAIL :</h2>
      {[data].map((el, idx) => {
        return (
          <div>
            {Object.entries(el).map((e, id) => {
              if (id !== 0) {
                return (
                  <h4>
                    {e[0]} : {e[1]}
                  </h4>
                );
              }
            })}
          </div>
        );
      })}

      <div>
        <Button style={{ marginRight: "5px" }} onClick={showModal}>
          edit
        </Button>
      </div>
      {/* -------------------------------------MODAL FOR ADD CUSTOMER------------------------------------------- */}
      <Modal show={editModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label> Produk: {data.produk} </Form.Label>
              <Form.Control type="text" autoFocus ref={nameRef} />

              <Form.Label> Harga: {data.harga} </Form.Label>
              <Form.Control type="text" autoFocus ref={priceRef} />

              <Form.Label> Deskripsi: {data.deskripsi} </Form.Label>
              <Form.Control type="text" autoFocus ref={descriptionRef} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={submitProduct}>
            Edit
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
