import React, { useEffect, useState, Fragment } from "react";
import {Button} from 'react-bootstrap'

export default function TabelData(props) {
  const [data, setData] = useState([]);
    useEffect(() => {
        setData(props.data);
    })

  const toDetail = (e) => {
    props.detail(e.id);
  };

  const deleteElement=(e)=>{
      console.log(e.target.value,"delete id from child ");
      props.del(e.target.value)
  }

  return (
    <tbody>
      {data.map((el, idx) => {
        return (
          <tr key={idx}>
            {Object.values(el).map((e, id) => {
              if (id === 0) {
                return <td>{idx + 1}</td>;
              }
              if (id === 1) {
                return (
                  <td>
                    <a
                    type="button"
                      value={e}
                      onClick={() => {
                        toDetail(el);
                      }}
                    >
                      {e}
                    </a>
                  </td>
                );
              } 
              else {return <td>{e}</td>};
            })}
            <td><Button onClick={deleteElement} value={el.id}>delete</Button></td>
          </tr>
        );
      })}
    </tbody>
  );
}
