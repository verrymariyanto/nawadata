import React from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import logo from "../assets/logo.png";
import { Link } from "react-router-dom";

export const NavbarWeb = () => {
  return (
    <>
      <Navbar bg="light" variant="light">
        <Container>
          <Navbar.Brand>
            <img src={logo} alt="#"></img>
          </Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link>
              <Link to="/"><h5>Home</h5></Link>
            </Nav.Link>
            <Nav.Link>
              <Link to="/Product"><h5>Product</h5></Link>
            </Nav.Link>
            <Nav.Link>
              <Link to="/Transaction"><h5>Transaction</h5></Link>
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
};
